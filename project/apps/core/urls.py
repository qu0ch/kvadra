from django.conf.urls import url
from core.views import MainView, ResultView

urlpatterns = [
    url(r'^$', MainView.as_view(), name=u'main'),
    url(r'^result/$', ResultView.as_view(), name=u'results'),
]