from django.forms import ModelForm

from core.models import Text


class TextForm(ModelForm):
    class Meta:
        model = Text
        fields = (u'content',)