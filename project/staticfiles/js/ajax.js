$(document).ready(function(){
    $(document).on("click", "button.submit", function(event){
        event.preventDefault()
        var csrftoken = getCookie('csrftoken');
        var content = $('textarea[name=content]', this.form).val()
        value = {'content': content,
                'csrfmiddlewaretoken':  csrftoken,
        }
        $.ajax({
            url: '/',
            type: "POST",
            data: value,
            dataType : "json",
            success: function (response) {
                if (response['status'] == "SUCCESS"){
                    alert(response['data'])
                    $('textarea[name=content]', this.form).val('')
                }
                else if (response['status'] == "FAIL"){
                    $('div#form').html(response['form']);
                }
            }
        })
    });
});

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

