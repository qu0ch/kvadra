# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.template.loader import render_to_string

from core.utils import JSONResponseMixin

from core.forms import TextForm
from core.models import Text


class BaseMethod():
    def uploadText(self, txt):
        content = Text(content=txt)
        content.save()


class MainView(TemplateView, BaseMethod, JSONResponseMixin):
    template_name = u'main.html'

    def post(self, request):
        form = TextForm(request.POST, request.FILES)
        if form.is_valid() and request.is_ajax():
            txt = form.cleaned_data[u'content']
            self.uploadText(txt)
            response = {u'status': u'SUCCESS',
                        u'data': u'Данные успешно добавлены'}
        else:
            context = {u'form': form}
            template = u'form.html'
            response = {u'status': u'FAIL',
                        u'form': render_to_string(template, context),}
        return self.render_to_json_response(context=response)

    def get(self, request):
        form = TextForm()
        context = {
            u'form': form,
        }
        return self.render_to_response(context=self.get_context_data(**context))


class ResultView(TemplateView):
    template_name = u'result.html'

    def getText(self):
        result = Text.objects.all()
        return result

    def get(self, request):
        texts = self.getText()
        context = {
            u'texts': texts,
        }
        return self.render_to_response(context=self.get_context_data(**context))