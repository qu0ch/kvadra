# -*- coding: utf-8 -*-
from django.db import models


class Text(models.Model):
    content = models.TextField(u'Текст с формы')

    class Meta:
        verbose_name = u'Текст'
        verbose_name_plural = u'Текст'
        # ordering = (u'-created',)

    def __unicode__(self):
        return unicode(self.content)
