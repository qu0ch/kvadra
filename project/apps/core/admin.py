from django.contrib import admin
from core.models import Text


class TextAdmin(admin.ModelAdmin):
    list_display = (u'id', u'content', )

admin.site.register(Text, TextAdmin)
